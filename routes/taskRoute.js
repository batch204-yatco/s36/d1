// Contains all endpoints for application
// We separate routes such that "index.js" only contains info on the server

const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController");

// Routes

// Route to get all tasks 

router.get("/", (req, res) => {


	// user defined
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))

});

router.post("/", (req,res) => {
	console.log(req.body)

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
});

router.delete("/:id", (req, res) => {
	console.log(req.params)

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));

});

// Route to update a task
router.put("/:id", (req, res) => {

	// {id: 633c229e762681f93cfc1e4c} 
	// console.log(req.params.id);
	// console.log(req.body)
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
}) 

// Route to get a specific task
router.get("/:id", (req, res) => {

	console.log(req.params)

	// user defined
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController))

});

// Route to change status to complete
router.put("/:id/complete", (req, res) => {

	console.log(req.params.id);
	console.log(req.body)
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
}) 


// Use "module.exports" to export router object to use in the "index.js" file
module.exports = router;
